//
//  AnimationSectionViewController.m
//  IOSProgrammerTest
//
//  Created by Justin LeClair on 12/15/14.
//  Copyright (c) 2014 AppPartner. All rights reserved.
//

#import "AnimationSectionViewController.h"
#import "MainMenuViewController.h"

@interface AnimationSectionViewController ()
@property BOOL logoIsTouched;
@property CGFloat iconStartingPoint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *iconVerticalConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *iconCenterXConstraint;
@property (strong, nonatomic) IBOutlet UIImageView *logoImageView;
@property (strong, nonatomic) IBOutlet UILabel *rotationsLabel;
@property (strong, nonatomic) IBOutlet UISlider *rotationSlider;
@end

@implementation AnimationSectionViewController

#pragma mark Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    // Used to update auto-layout constraint in touchesMoved
    self.iconStartingPoint = self.logoImageView.center.y;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark IBActions

- (IBAction)backAction:(id)sender
{
    // To prevent memory leaks go back to root view controller
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)spinAction:(id)sender
{
    [self spinView:self.logoImageView
      numRotations:(int)self.rotationSlider.value
          duration:(self.rotationSlider.value / 4) + 1];
}

- (IBAction)slideAction:(UISlider *)sender
{
    int roundedValue = sender.value;
    [sender setValue:roundedValue animated:NO];
    self.rotationsLabel.text = [NSString stringWithFormat:@"Rotations: %i", roundedValue];
}

#pragma mark Helper Methods

- (void)spinView:(UIView *)viewToSpin numRotations:(int)rotations duration:(float)duration
{
    CAKeyframeAnimation *rotationAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.z"];
    
    // Create keyframes to spin view
    NSMutableArray *values = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < (rotations * 2) + 1; i++)
    {
        NSNumber *value = @(i * M_PI);
        [values addObject:value];
    }
    
    rotationAnimation.values = values;
    rotationAnimation.calculationMode = kCAAnimationPaced;
    rotationAnimation.removedOnCompletion = YES;
    rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    rotationAnimation.duration = duration;
    
    [self.logoImageView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

- (void)flashIcon
{
    // Create a flash effect when user stops touching app icon
    CGFloat buffer = 10.0;
    UIImageView *flashView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_apppartner_edit"]];
    flashView.center = CGPointMake(self.logoImageView.center.x, self.logoImageView.center.y - buffer);
    flashView.alpha = 0.0;
    
    [self.view insertSubview:flashView belowSubview:self.logoImageView];
    
    [UIView animateWithDuration:0.0 delay:0.15 options:UIViewAnimationOptionCurveEaseOut animations:^{
        flashView.alpha = 0.5;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [UIView setAnimationRepeatCount:1];
            flashView.transform = CGAffineTransformMakeScale(1.5, 1.5);
            flashView.alpha = 0.0;
        } completion:^(BOOL finished) {
            [flashView removeFromSuperview];
        }];
    }];
}

#pragma mark Touch Events

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if (CGRectContainsPoint(self.logoImageView.frame, touchLocation))
    {
        self.logoIsTouched = YES;
        self.logoImageView.transform = CGAffineTransformMakeScale(1.2, 1.2);
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.logoIsTouched)
    {
        for (UITouch *touch in touches)
        {
            CGPoint location = [touch locationInView:self.view];
            self.iconCenterXConstraint.constant = -(location.x - (self.view.frame.size.width / 2));
            self.iconVerticalConstraint.constant = location.y - self.iconStartingPoint;
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.logoIsTouched)
    {
        [self flashIcon];
        [UIView animateWithDuration:0.5 delay:0.0 usingSpringWithDamping:0.5 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseOut
             animations:^{
                self.logoImageView.transform = CGAffineTransformIdentity;
             } completion:^(BOOL finished) {
             }];
        self.logoIsTouched = NO;
    }
}


- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
