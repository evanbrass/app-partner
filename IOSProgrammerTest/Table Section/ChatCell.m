//
//  TableSectionTableViewCell.m
//  IOSProgrammerTest
//
//  Created by Justin LeClair on 12/15/14.
//  Copyright (c) 2014 AppPartner. All rights reserved.
//

#import "ChatCell.h"

@interface ChatCell ()
@property (strong, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, strong) IBOutlet UILabel *usernameLabel;
@property (nonatomic, strong) IBOutlet UITextView *messageTextView;
@end

@implementation ChatCell

- (void)awakeFromNib
{
    // Stylize imageview
    self.avatarImageView.layer.cornerRadius = self.avatarImageView.layer.frame.size.height / 2;
    self.avatarImageView.clipsToBounds = YES;
}

- (void)loadWithData:(ChatData *)chatData
{
    self.usernameLabel.text = chatData.username;
    self.messageTextView.text = chatData.message;
    
    // Set image asynchronously
    NSURL *avatarURL = [NSURL URLWithString:chatData.avatar_url];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        UIImage *imageFromURL = [UIImage imageWithData:[NSData dataWithContentsOfURL:avatarURL]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.avatarImageView setImage: imageFromURL];
        });
    });
}

@end
