//
//  APISectionViewController.m
//  IOSProgrammerTest
//
//  Created by Justin LeClair on 12/15/14.
//  Copyright (c) 2014 AppPartner. All rights reserved.
//

#import "LoginSectionViewController.h"
#import "MainMenuViewController.h"

#define LOGIN_URL @"http://dev.apppartner.com/AppPartnerProgrammerTest/scripts/login.php"

@interface LoginSectionViewController () <NSURLConnectionDelegate, NSURLConnectionDataDelegate,
                                          UIAlertViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) IBOutlet UITextField *usernameTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property float requestStartTime;
@end

@implementation LoginSectionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.usernameTextField.delegate = self;
    self.passwordTextField.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark IBActions

- (IBAction)backAction:(id)sender
{
    // To prevent memory leaks go back to root view controller
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)loginButtonAction:(UIButton *)sender
{
    NSString *postRequest = [NSString stringWithFormat:@"username=%@&password=%@",
                             self.usernameTextField.text, self.passwordTextField.text];
    NSData *requestData = [postRequest dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[requestData length]];
    NSURL *requestURL = [NSURL URLWithString: LOGIN_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:requestData];
    [request setTimeoutInterval:60];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.requestStartTime = CFAbsoluteTimeGetCurrent();
    
    if (!connection)
    {
        NSLog(@"Error establishing connection");
    }
}

#pragma mark NSURLConnection

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // Display alert view in case of fail
    NSString *errorMessage = [NSString stringWithFormat:@"%@", error];
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Connection Failed!"
                                                         message: errorMessage delegate:nil
                                               cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [errorAlert show];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	NSDictionary *requestDict = [NSJSONSerialization JSONObjectWithData:data
	                                                            options:NSJSONReadingMutableContainers
	                                                              error:nil];
	// Get the duration and values of the connection request
	float result = fabs(self.requestStartTime - (CFAbsoluteTimeGetCurrent()));
	NSString *codeString = [requestDict objectForKey:@"code"];
	NSString *messageString = [requestDict objectForKey:@"message"];

	NSString *alertMessage = [NSString stringWithFormat:@"%@ time: %.2f milliseconds", messageString, result];

	// Display success alert view
	UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:codeString
	                                                       message:alertMessage
	                                                      delegate:nil
	                                             cancelButtonTitle:@"OK"
	                                             otherButtonTitles:nil, nil];
	successAlert.delegate = self;
	[successAlert show];
}

#pragma mark UIAlertView

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ([alertView.title isEqualToString:@"Success"])
    {
        [self backAction:@"Success"];
    }
}

#pragma mark UITextField

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // Hide keyboard when return is pressed
    [textField resignFirstResponder];
    return YES;
}

#pragma mark Touch Events

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // Hide keyboard if view is pressed
    [self.passwordTextField resignFirstResponder];
    [self.usernameTextField resignFirstResponder];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}



@end
